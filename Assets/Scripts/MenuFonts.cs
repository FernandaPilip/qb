﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuFonts : MonoBehaviour {

    public List<Text> Texts;
    public int FontSize;

	void Awake() {
        StartCoroutine(WaitForFont());
	}

    IEnumerator WaitForFont()
    {
        yield return new WaitForEndOfFrame();
        foreach (Text text in GetComponentsInChildren<Text>())
        {
            if (FontSize == 0 || text.cachedTextGenerator.fontSizeUsedForBestFit < FontSize)
                FontSize = text.cachedTextGenerator.fontSizeUsedForBestFit;
            Texts.Add(text);
        }
        foreach (Text text in Texts)
        {
            text.resizeTextForBestFit = false;
            text.fontSize = FontSize;
        }
        transform.SetAsLastSibling();
    }
}
