﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using UnityEngine.UI;

public class AdManager : MonoBehaviour
{

    public static AdManager Instance;

    InterstitialAd interstitial;
    InterstitialAd prevInterstitial;

    [SerializeField]
    private Text Logger;

    public int Tries;

    private void Awake()
    {
        Instance = this;
    }

    public void AddTry()
    {
        Tries = PlayerPrefs.GetInt("Tries", 0);
        Tries++;
        PlayerPrefs.SetInt("Tries", Tries);
        if (Tries >= 5)
        {
            ShowAd();
        }
    }
    
    public void RequestInterstitial()
    {
        #if UNITY_ANDROID
            string adUnitId = "ca-app-pub-3940256099942544/1033173712";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/4411468910";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        interstitial = new InterstitialAd(adUnitId);
        // Called when an ad request has successfully loaded.
        interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        interstitial.OnAdClosed += HandleOnAdClosed;

        AdRequest request  = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
        //Canvaser.Instance.ScoreText.text = "Ad requested";
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        RequestInterstitial();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        //Canvaser.Instance.ScoreText.text = "HandleAdLoaded event received";
        prevInterstitial = interstitial;
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //Canvaser.Instance.ScoreText.text = "HandleFailedToReceiveAd event received with message: "
        //                    + args.Message;
        interstitial = prevInterstitial;
    }
    public void ShowAd()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
            PlayerPrefs.SetInt("Tries", 0);
        }
        else
        {
            //Canvaser.Instance.ScoreText.text = "NOT LOADED(((";
        }
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        RequestInterstitial();
    }
}
