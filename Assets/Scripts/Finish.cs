﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour {

    public Player player;
    public GameObject GameOver;

    void OnTriggerEnter(Collider other)
    {
        player.GameOver = true;
        GameOver.SetActive(true);
    }
}
