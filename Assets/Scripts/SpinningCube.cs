﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpinningCube : MonoBehaviour
{

    public Vector3 SpinningAngle;
    public float Speed = 1f;
    public Animation Q;
    public Animation B;
    public Animation Cube;
    public Animation Fader;
    public AudioSource AS;

    bool Starting;

    void Start()
    {
        AS.volume = PlayerPrefs.GetInt("Volume",1);
        StartCoroutine(FadeIn());
    }

    public void StartGame()
    {
        Fader.gameObject.SetActive(true);
        Q.Play("Q");
        B.Play("B");
        Cube.Play("Cube");
        Fader.Play("FaderOut");
        Starting = true;
        StartCoroutine(WaitAndLoad());
    }

    IEnumerator FadeIn()
    {
        Fader.gameObject.SetActive(true);
        Fader.Play("FaderIn");
        yield return new WaitForSeconds(1f);
        Fader.gameObject.SetActive(false);
    }

    IEnumerator WaitAndLoad()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(1);
    }

    void Update()
    {
        transform.Rotate(SpinningAngle * Speed * Time.deltaTime);

        if (Starting)
            AS.volume -= 0.5f * Time.deltaTime;

        if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
        }
    }
}
