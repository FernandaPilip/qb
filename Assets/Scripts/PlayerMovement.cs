﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float MovementPause = 0.05f;
    public float SpeedMultiplier = 10;
    public float GravityMultiplier = 3;

    public static PlayerMovement Instance;
    
    private float _currentRotation = 0;
    private bool rotate = true;
    private Vector3 direction = Vector3.forward;
    private bool jump = false;
    private int counter = 0;
    private bool canChangeDir = true;
    
    public Canvaser can;

    public MapGenerator MG;
    
    RaycastHit hit;

    public Transform currentBlock;

    public AudioClip AC;
    public AudioSource AS;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        can = Canvaser.Instance;
        MG = can.MG;
    }

    Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    void Update()
    {
        if (!can.GameOver)
        {
            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                Jump();
            }
            if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                ChangeDirection();
            }
            if (rotate)
            {
                var angle = 90;

                if (jump)
                {
                    if (counter < 1)
                    {
                        angle = 180;
                        counter++;
                    }
                    else
                    {
                        counter = 0;
                        jump = false;
                    }
                }
                StartCoroutine(RotationAround(direction, angle));
            }

            CheckDeath();
        }
    }

    void CheckDeath()
    {
        if (transform.position.y < 0)
        {
            can.GameFinish();
        }
    }

    public void Jump()
    {
        jump = true;
    }

    public void ChangeDirection()
    {
        if (!canChangeDir) return;

        if (direction == Vector3.forward)
        {
            direction = Vector3.right;
        }
        else
        {
            direction = Vector3.forward;
        }

        canChangeDir = false;
    }

    IEnumerator RotationAround(Vector3 rotation, float finalAngle)
    {
        rotate = false;
        bool stop = false;
        float rotationAngle = 0;

        var yVector = jump && counter == 1 ? Vector3.zero : new Vector3(0, -0.5f, 0);
        var multiplier = yVector == Vector3.zero ? 1f : 0.5f;

        Vector3 pivot = Position + yVector + (new Vector3(1, 0, 1) - rotation) * multiplier;

        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (_currentRotation < finalAngle)
            {
                rotationAngle = SpeedMultiplier * Time.deltaTime;
            }
            else
            {
                AS.PlayOneShot(AC);
                rotationAngle = finalAngle - _currentRotation;
                stop = true;
                can.SetScore(true);
                if(SpeedMultiplier < 700)
                {
                    SpeedMultiplier += 0.5f;
                }
            }

            if (rotation.z > 0)
            {
                rotationAngle = -rotationAngle;
            }

            transform.RotateAround(pivot, rotation, rotationAngle);
            _currentRotation += Mathf.Abs(rotationAngle);

            if (stop)
            {
                break;
            }
        }

        FixPosition();
        _currentRotation = 0;
        canChangeDir = true;


        StartCoroutine(WaitTime());
    }

    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(MovementPause);
        rotate = true;
        CheckUnder();
    }

    void FixPosition()
    {
        Position = new Vector3((float)Math.Round(Position.x, 1, MidpointRounding.AwayFromZero), (float)Math.Round(Position.y, 1, MidpointRounding.AwayFromZero), (float)Math.Round(Position.z, 1, MidpointRounding.AwayFromZero));
    }
    

    void CheckUnder()
    {

        if (!Physics.Raycast(new Ray(Position, Vector3.down), out hit, 2))
        {
            rotate = false;
            StartCoroutine(ArtificialGravity());
        }
        else
        {
            if(hit.transform != currentBlock)
            {
                MG.NextBlock();
                currentBlock.GetComponentInParent<BlockDestroyer>().Fall();
                currentBlock = hit.transform;
            }
        }
    }

    IEnumerator ArtificialGravity()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            transform.Translate(Vector3.down * Time.deltaTime * GravityMultiplier, Space.World);
        }
    }

}