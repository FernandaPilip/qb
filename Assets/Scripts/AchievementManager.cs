﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public struct Achievement
{
    public bool OnFinish;
    public string text;
    public string number;

    public Achievement(bool onFinish, string _text,string _number)
    {
        OnFinish = onFinish;
        text = _text;
        number = _number;
    }
}

public class AchievementManager : MonoBehaviour
{

    public static Dictionary<string, Achievement> Achievements = new Dictionary<string, Achievement>();

    void Start()
    {
        if (Achievements.Count == 0)
        {
            ReadXML();
        }
    }

    void ReadXML()
    {
        XmlDocument xml = new XmlDocument();
        string text = (Resources.Load("Achievements") as TextAsset).text;
        xml.LoadXml(text);
        XmlElement root = xml.DocumentElement;
        foreach (XmlNode node in root)
        {
            Achievement sas = new Achievement(bool.Parse(node.Attributes.GetNamedItem("OnFinish").Value), node.InnerText, node.Name.Substring(1));
            Achievements.Add(node.Name.Substring(1), sas);
        }
    }

    public static List<Achievement> GetAchievements()
    {
        int HighScore = PlayerPrefs.GetInt("HighScore", 0);
        List<Achievement> achievements = new List<Achievement>();
        foreach (var item in Achievements)
        {
            if(int.Parse(item.Key) <= HighScore)
            {
                achievements.Add(item.Value);
            }
        }

        return achievements;
    }
}
