﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDestroyer : MonoBehaviour
{
    public float GravityMultiplier = 100;
    public float timeToDestroy = 5;
    float timer;

    
    public void Fall()
    {
        StartCoroutine(ArtificialGravity());
    }

    IEnumerator ArtificialGravity()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            timer += Time.deltaTime;
            if (timer >= timeToDestroy)
                break;
            transform.Translate(Vector3.down * Time.deltaTime * GravityMultiplier, Space.World);
        }
        Destroy(gameObject);
    }
}
