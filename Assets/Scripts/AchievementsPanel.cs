﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsPanel : MonoBehaviour {

    public Transform Content;
    public GameObject AchievementObject;
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ClearContent()
    {
        foreach (Transform item in Content)
        {
            Destroy(item.gameObject);
        }
    }

    public void SetAchievements(List<Achievement> achievements)
    {
        ClearContent();
        foreach (Achievement item in achievements)
        {
            GameObject newA = Instantiate(AchievementObject,Content);
            newA.GetComponentsInChildren<Text>()[0].text = item.text;
            newA.GetComponentsInChildren<Text>()[1].text = item.number;
        }
    }
}
