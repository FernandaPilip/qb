﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Canvaser : MonoBehaviour
{
    public static Canvaser Instance;

    public bool GameOver = true;
    public GameObject GameOverPanel;
    public Text ScoreText;
    public int Score;
    public GameObject Menu;
    public GameObject Fader;

    public Text HighScoreOnFinish;


    public MapGenerator MG;

    public GameObject AchievementPanel;
    public Text AchievementText;

    public bool AchievementShown;

    public GameObject HighScorePanel;
    public Text HighScore;

    public AchievementsPanel achievementsPanel;
    public GameObject AchievementsPanelObject;

    public TouchReader TR;

    public List<AudioSource> Audios;

    public GameObject SettingsPanel;
    public Slider Volume;

    public int TriesCount;

    public List<GameObject> Panels;
    public int ActivePanelIndex;

    public Toggle VolumeToggle;

    public GameObject TurnLeftTutorial;
    public GameObject TurnRightTutorial;
    public GameObject JumpTutorial;
    public GameObject TutorialPanel;
    public bool IsTutorial;
    float tempSpeed;
    int tutSlowCount = 0;


    private void Awake()
    {
        Instance = this;
        //PlayerPrefs.DeleteKey("NotFirstTime");
        if (PlayerPrefs.GetInt("NotFirstTime", 0) == 0)
        {
            IsTutorial = true;
            PlayerPrefs.SetInt("NotFirstTime", 1);
        }
        else
        {
            TutorialPanel.SetActive(false);
        }
    }

    void Start()
    {
        GameOver = false;
        StartCoroutine(WaitForFade());
        SetAudios();
        SetAudioVolume(PlayerPrefs.GetInt("Volume", 1));
        VolumeToggle.isOn = (PlayerPrefs.GetInt("Volume", 1) == 0);
    }

    public void SetAudios()
    {
        foreach (AudioSource item in FindObjectsOfType<AudioSource>())
        {
            Audios.Add(item);
        }
    }

    public void SetVolume(bool vol)
    {
        int volume = vol ? 0 : 1;
        SetAudioVolume(volume);
        PlayerPrefs.SetInt("Volume", volume);
    }

    public void SetAudioVolume(int vol)
    {
        foreach (AudioSource item in Audios)
        {
            item.volume = vol;
        }
    }

    IEnumerator WaitForFade()
    {
        Fader.SetActive(true);
        yield return new WaitForSeconds(3f);
        Fader.SetActive(false);
    }

    public void SetSpeed(int speed = 0)
    {
        if (speed == 1)
        {
            tempSpeed = PlayerMovement.Instance.SpeedMultiplier;
            PlayerMovement.Instance.SpeedMultiplier = 20;
            tutSlowCount++;
        }
        else
        {
            PlayerMovement.Instance.SpeedMultiplier = tempSpeed + 0.5f;
            if (tutSlowCount == 3)
            {
                TurnRightTutorial.SetActive(false);
                IsTutorial = false;
                TutorialPanel.SetActive(false);
                tutSlowCount = 0;
            }
            else if (tutSlowCount == 2)
            {
                JumpTutorial.SetActive(false);
            }
            else if (tutSlowCount == 1)
            {
                TurnLeftTutorial.SetActive(false);
            }
        }
    }

    public void SetScore(bool toAdd)
    {
        if (toAdd)
        {
            Score++;
            if (IsTutorial)
            {
                if (Score == 11)
                {
                    SetSpeed(1);
                    TurnLeftTutorial.SetActive(true);
                }
                else if (Score == 18)
                {
                    SetSpeed(1);
                    JumpTutorial.SetActive(true);
                }
                else if (Score == 27)
                {
                    SetSpeed(1);
                    TurnRightTutorial.SetActive(true);
                }
            }
            if (!AchievementShown)
            {
                ScoreText.text = Score.ToString();
            }
            CheckAchievement(Score.ToString(), false);
        }
        else
        {
            Score = 0;
        }
    }

    void CheckAchievement(string score, bool isFinish)
    {
        if (PlayerPrefs.GetInt("HighScore", 0) < Score)
        {
            if (AchievementManager.Achievements.ContainsKey(score))
            {
                if (AchievementManager.Achievements[score].OnFinish == isFinish)
                {
                    if (AchievementPanel.activeInHierarchy)
                    {
                        AchievementPanel.SetActive(false);
                        StopCoroutine(AchevementCloser());
                    }
                    AchievementText.text = AchievementManager.Achievements[score].text;
                    StartCoroutine(AchevementCloser());
                }
            }
        }
    }

    IEnumerator AchevementCloser()
    {
        AchievementPanel.SetActive(true);
        AchievementShown = true;
        yield return new WaitForSeconds(1f);
        ScoreText.text = Score.ToString();
        AchievementShown = false;
        yield return new WaitForSeconds(1f);
        AchievementPanel.SetActive(false);
    }

    public void Back()
    {
        if (Panels[ActivePanelIndex].activeInHierarchy)
        {
            if (ActivePanelIndex == 0)
            {
                GoToMenu();
            }
            else
            {
                Panels[ActivePanelIndex].SetActive(false);
                ActivePanelIndex = 0;
                Menu.SetActive(true);
            }
        }
        else
        {
            Time.timeScale = Menu.activeInHierarchy ? 1 : 0;
            TR.col.enabled = Menu.activeInHierarchy;
            Menu.SetActive(!Menu.activeInHierarchy);
            GameOverPanel.SetActive(!Menu.activeInHierarchy && GameOver);
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            Back();
        }
    }

    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GoToMenu()
    {
        Time.timeScale = 1;
        Application.Quit();
        //SceneManager.LoadScene("Main");
    }

    public void GameFinish()
    {
        GameOver = true;
        if(IsTutorial)
        {
            TutorialPanel.SetActive(false);
            PlayerPrefs.SetInt("NotFirstTime", 0);
        }
        ActivatePanel(3);
        CheckAchievement(ScoreText.text, true);

        if (Score > PlayerPrefs.GetInt("HighScore", 0))
            PlayerPrefs.SetInt("HighScore", Score);

        HighScoreOnFinish.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        AdManager.Instance.AddTry();
    }

    public void OpenHighScore()
    {
        HighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        ActivatePanel(1);
    }

    public void ResetHighScore()
    {
        PlayerPrefs.SetInt("HighScore", 0);
        HighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        ActivatePanel(0);
    }

    public void OpenAchievements()
    {
        ActivatePanel(2);
        achievementsPanel.SetAchievements(AchievementManager.GetAchievements());
    }

    public void ActivatePanel(int index)
    {
        Panels[ActivePanelIndex].SetActive(false);
        Panels[index].SetActive(true);
        ActivePanelIndex = index;
    }

    public void OpenSettings()
    {
        SettingsPanel.gameObject.SetActive(true);
        Volume.value = PlayerPrefs.GetFloat("Volume", 1);
        Menu.SetActive(false);
    }
}
