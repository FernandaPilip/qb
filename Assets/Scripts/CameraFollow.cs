﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform Target;
    public float speed = 1;
    
    private Vector3 offset;

    private void Start()
    {
        offset = Target.position - transform.position;
    }

    void Update()
    {
        Vector3 pos = Target.position - offset;
        pos.y = transform.position.y;
        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * speed);
    }
}
