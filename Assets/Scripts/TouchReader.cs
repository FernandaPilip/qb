﻿using System.Collections;
using UnityEngine;

public class TouchReader : MonoBehaviour
{
    public Collider col;

    [SerializeField]
    private float sqrMag = 1;

    private bool SwipeRead;
    private PlayerMovement PM;
    private MapGenerator MG;
    private Vector3 pos;
    private Vector2 curPos;

    void Start()
    {
        sqrMag = Mathf.Pow((Screen.width < Screen.height ? Screen.width : Screen.height) / 6, 2);
        PM = PlayerMovement.Instance;
    }

    void OnMouseDown()
    {
        pos = Input.mousePosition;
        SwipeRead = true;
    }

    void OnMouseDrag()
    {
        curPos = Input.mousePosition - pos;
        if (curPos.sqrMagnitude > sqrMag && SwipeRead)
        {
            MakeMove(curPos);
        }

    }

    void OnMouseUp()
    {
        if (SwipeRead)
        {
            MakeMove(Input.mousePosition - pos);
        }
    }

    void MakeMove(Vector2 curDelta)
    {
        if (Mathf.Abs(curDelta.x) > curDelta.y)
        {
            PM.ChangeDirection();
        }
        else
        {
            PM.Jump();
        }
        if (Canvaser.Instance.IsTutorial)
        {
            Canvaser.Instance.SetSpeed();
        }
        SwipeRead = false;
    }
}
