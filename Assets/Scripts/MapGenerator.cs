﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    public GameObject StartBlock;
    public List<GameObject> Blocks;
    public List<GameObject> GeneratedBlocks;
    public int BlockCount;
    Vector3 LastPos;
    Vector3 NewDir;
    [SerializeField]
    bool left = true;

    public struct BlockData
    {
        public int size;
        public int makePit;
    }
    
	void Start () {
        PlaceBlocks();
	}

    public void PlaceBlocks(List<BlockData> data)
    {
        PlaceBlock(Blocks.Count - 1, 0);
        PlayerMovement.Instance.currentBlock = GeneratedBlocks[0].transform;
        foreach (BlockData block in data)
        {
            PlaceBlock(block.size, block.makePit);
        }
    }

    public void PlaceBlocks()
    {
        PlaceStartBlock();
        PlayerMovement.Instance.currentBlock = GeneratedBlocks[0].transform.GetChild(0);
        if (Canvaser.Instance.IsTutorial)
        {
            PlaceBlock(Blocks.Count - 1, 1);
            PlaceBlock(Blocks.Count - 1, 0);
            PlaceStartBlock();
        }
        for (int i = 0; i < BlockCount; i++)
        {
            PlaceBlock();
        }
    }

    public void PlaceBlock()
    {
        PlaceBlock(Random.Range(0, Blocks.Count), Random.Range(0, 2));
    }

    public void NextBlock()
    {
        GeneratedBlocks.RemoveAt(0);
        PlaceBlock();
    }

    public void Restart()
    {
        LastPos = Vector3.zero;
        PlayerMovement.Instance.transform.position = Vector3.up;
        PlaceBlocks();
    }

    public void PlaceStartBlock()
    {
        int size = int.Parse(StartBlock.name);
        GameObject newBlock = Instantiate(StartBlock);
        newBlock.transform.SetParent(transform);
        newBlock.transform.position = LastPos;
        if (left)
            NewDir.Set(0, 0, size + 2);
        else
            NewDir.Set((size), 0, 0);


        LastPos += NewDir;
        newBlock.transform.rotation = Quaternion.Euler(0, 90 * (left ? 0 : 1), 0);
        left = !left;
        GeneratedBlocks.Add(newBlock);
    }

    public void PlaceBlock(int size, int makePit)
    {
        GameObject newBlock = Instantiate(Blocks[size]);
        newBlock.transform.SetParent(transform);
        newBlock.transform.position = LastPos;

        int step = int.Parse(Blocks[size].name);

        if (left)
            NewDir.Set(0, 0, step + makePit);
        else
            NewDir.Set((step + makePit), 0, 0);

        
        LastPos += NewDir;
        newBlock.transform.rotation = Quaternion.Euler(0, 90 * (left ? 0 : 1), 0);

        if (makePit == 0)
        {
            left = !left;
        }


        GeneratedBlocks.Add(newBlock);
        //newBlock.GetComponentInChildren<BlockDestroyer>().MG = this;
    }
    
}
