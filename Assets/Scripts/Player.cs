﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Rigidbody RB;
    public bool GameOver;
    public Animator anim;

    public float speed = 6;

    Vector3 moveTo = new Vector3(0, 0, 1f);

    void Start () {
        RB = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        anim.speed = speed;
	}
	
	void Update () {

        if (!GameOver)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                moveTo.Set(0, 0, 1f);
                anim.SetBool("IsRight", true);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                moveTo.Set(-1f, 0, 0);
                anim.SetBool("IsRight", false);
            }
        }

        transform.Translate(moveTo * Time.deltaTime * speed);
    }
    
}
